const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports.register = (req, res, next) => {
    console.log('Sign up Fn called');
    var user = new User(
        {
            name : req.body.name,
            email : req.body.email,
            password : req.body.password
        }
    );

    user.save((err, doc) => {
        if(!err) 
        res.send(doc);
        else{
            console.log(err)

            if(err.code === 11000){
                res.status(422).send('Email already exist!');
            }else{
                return next(err);
            }
        }
    });

}