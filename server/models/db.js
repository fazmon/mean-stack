const { default: mongoose } = require("mongoose")

mongoose.connect(process.env.MONGODB_URI, (err) => {
    if(!err) { console.log('connection successful')}
    else{
        console.log("Connection Error "+ JSON.stringify(err, undefined, 2));
    }
});


require('./user.model');
