
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');


var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: 'Name is mandatory'
    },
    email: {
        type: String,
        required: 'Email is mandatory',
        unique: true
    },
    password: {
        type: String,
        required: 'Password is mandatory'
    },
    saltSecret: {
        type: String
    }
});


userSchema.pre('save', function(next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});

mongoose.model('User', userSchema);
